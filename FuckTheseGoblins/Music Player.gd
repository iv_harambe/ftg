
extends Container

var cancion_actual = 0 


func _on_selector_de_canciones_button_selected( button_idx ):
	if(button_idx):
		siguiente_cancion()
	else:
		cancion_anterior()
	elegir_musica(cancion_actual)

func siguiente_cancion():
	cancion_actual +=1
	cancion_actual = cancion_actual%game_resources.music.size()


func cancion_anterior():
	if cancion_actual == 0: 
		cancion_actual = game_resources.music.size() - 1
	else:
		cancion_actual -=1

func elegir_musica(musica_elegida):
	get_node("StreamPlayer").set_stream(game_resources.music[musica_elegida])
	get_node("StreamPlayer").play()
	get_node("Music_name").set_text(game_resources.music_name[musica_elegida])

