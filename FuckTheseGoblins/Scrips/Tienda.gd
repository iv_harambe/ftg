
extends Control

var pagina_actual = 0


func _ready():
	pagina_actual = 0
	get_node("StreamPlayer").play(0.0)
	var page = Spells_Resourse.pages[pagina_actual]
	page.set_pos(Vector2(50,0))
	page.set_name("sc")
	add_child(page)
	set_fixed_process(true)
		
	


func _on_volver_pressed():
	get_node("StreamPlayer").stop()
	remove_child(get_node("sc"))
	get_tree().change_scene("res://Level_menu.xml")
	
	

func _fixed_process(delta):
	get_node("Mocos").set_text("Mocos En Bolsa : " + str(game_state.goblin_mucus))
	


func _on_Button_Page_I_pressed():
	if pagina_actual == 0:
		pass
	else:
		remove_child(get_node("sc"))
		pagina_actual = pagina_actual - 1
		var page = Spells_Resourse.pages[pagina_actual]
		page.set_pos(Vector2(50,0))
		page.set_name("sc")
		add_child(page)


func _on_Button_Page_D_pressed():
	
	if pagina_actual == Spells_Resourse.pages.size()-1:
		pass
	else:
		remove_child(get_node("sc"))
		pagina_actual = pagina_actual + 1
		var page = Spells_Resourse.pages[pagina_actual]
		page.set_pos(Vector2(50,0))
		page.set_name("sc")
		add_child(page)
