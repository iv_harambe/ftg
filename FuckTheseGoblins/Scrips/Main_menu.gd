extends Control

var cancion_actual = 0

func _ready():
	get_node("Background").play("Easy")
	get_node("StreamPlayer").play()
	#inicializar_musica()
	
func inicializar_musica():
	var mp = game_resources.music_player.instance()
	add_child(mp)
	mp.elegir_musica(cancion_actual)

func _on_Start_pressed():
	get_tree().change_scene("res://FTG.xml")

func _on_Dificultad_button_selected( button_idx ):
	game_state.dificulty = (button_idx+1)*2
	get_node("Descripcion_L1").set_text(game_resources.linea1[button_idx])
	get_node("Descripcion_L2").set_text(game_resources.linea2[button_idx])
	get_node("Start").set_disabled(false)
	get_node("Background").play(game_resources.dificultad[button_idx])



func _on_Button_pressed():
	get_tree().quit()
	queue_free()
