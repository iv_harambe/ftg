extends RigidBody2D

var cooldown = 5.0
var time_to_explode = 1.8
var damage = 100
var objects_in_area = []
onready var spell_area = get_node("Area2D")
onready var timer_explode = get_node("timer_to_explode")


func shoot(player,mira):
		_place(mira.get_pos())


func _place(target_position):
	set_pos(target_position)

func get_cooldown():
	return cooldown

func _ready():
	set_fixed_process(true)
	get_node("spriteBomb/spellr_anim").play("idle")
	timer_explode.set_wait_time(time_to_explode)
	timer_explode.start()
	#spell_area.set_monitorable(false)

func _fixed_process(delta):
	pass

func _on_timer_to_explode_timeout():
	#spell_area.set_monitorable(true)
	get_node("spriteBomb/spellr_anim").play("explode")

func impacte_goblin(goblin):
	pass

func explode_finish():
	for body in objects_in_area :
		if body.has_method("recibir_danho"):
			body.recibir_danho(damage)
	self.queue_free()

func _on_Area2D_body_enter( body ):
	print("entro goblin")
	if (not body in objects_in_area):
		objects_in_area.append( body )

func _on_Area2D_body_exit( body ):
	print("salio goblin")
	if body in objects_in_area:
		objects_in_area.erase( body )
		

func recibir_danho( damage ):
	pass