
extends Node2D

# Extern resources
var genera_gob = preload("res://Generador_de_gob.xml")
var vanishing_text = preload("res://vanishing_text.xml")
var goblin_mucus_texture = preload("res://Sprites/goblin_mucus.png")
var fruit = preload("res://Fruit.xml")

# Children nodes
onready var tilemap = get_node("Navigation2D/TileMapFloor")
onready var tilemap_tree = get_node("TileMapTree")
onready var player = get_node("YSort/player")
onready var ysort = get_node("YSort")

var long_mapa = 100
var dificultad = 1
var nivel = 1
var posicion = Vector2(10,10)

var amount_goblins_to_spawn = null
var wave_end_reached = false

var not_to_use_positions = []

func _ready():
	wave_end_reached = false
	set_fixed_process(true)
	set_mouse_invisible()
	create_map()
	update_game_state_values()
	get_node("YSort/player").set_pos(get_pos_for_player())
	get_node("ParallaxBackground/NivelSprite/Nivel").set_text("Nivel: " + str(nivel))
	get_node("WaveManager").start_wave(nivel)
	get_node("ParallaxBackground/Spells")._ready()

func _fixed_process(delta):
	if Input.is_action_pressed("ui_cancel"):
		on_ESC_pressed()
	if Input.is_action_pressed("reset_level"):
		get_tree().change_scene("res://FTG.xml")
		queue_free()


func update_game_state_values():
	game_state.killed_goblins = 0
	nivel = game_state.level
	dificultad = game_state.dificulty

	get_node("GoblinGenerator").max_width = game_state.map_max_width
	get_node("GoblinGenerator").max_height = game_state.map_max_height
	game_state.goblins = 0

func terminar_nivel():
	nivel += 1
	game_state.level = nivel
	get_node("ParallaxBackground/NivelSprite/Nivel").set_text("nivel: " + str(nivel))

func create_map():
	var map_generation_node = load("map_generation.xml").instance()
	map_generation_node.setup(nivel, tilemap, tilemap_tree, ysort)
	add_child(map_generation_node)
	var map_filler = get_node("MapFiller")
	not_to_use_positions = map_filler.used_tiles
	print(not_to_use_positions)

func get_pos_for_player():
	var map_filler = get_node("MapFiller")
	var to_use_position = map_filler.generate_random_pos()
	while to_use_position in not_to_use_positions:
		to_use_position = map_filler.generate_random_pos()
	print(to_use_position)
	return map_filler.get_tile_center_pos(to_use_position)

func _on_player_dead_player():
	set_mouse_visible()
	get_tree().change_scene("res://Dead_menu.xml")

func dead_goblin(death_pos, mucus_amount):
	game_state.killed_goblins += 1
	game_state.puntaje += 500
	game_state.goblin_mucus += mucus_amount

	var puntaje = game_state.puntaje
	get_node("ParallaxBackground/Puntaje").set_text("Puntaje: " + str(puntaje))

	if wave_end_reached:
		if game_state.killed_goblins == game_state.goblins:
			terminar_nivel()
			abrir_menu_lvl()

	var new_text = vanishing_text.instance()
	new_text.setup(mucus_amount, goblin_mucus_texture, death_pos)
	add_child(new_text)

	randomize()
	if (randf() < 0.3) and (player.vida < player.vida_max):
		var new_fruit = fruit.instance()
		new_fruit.set_pos(death_pos)
		add_child(new_fruit)

func crear_invocadores(pos):
	var ggob = genera_gob.instance()
	ggob.set_pos(pos)
	ggob.presa = player
	add_child(ggob)

func agregar_goblin(goblin):
	goblin.setup(get_node("Navigation2D"), player)
	get_node("YSort").add_child(goblin)

func abrir_menu_lvl():
	get_tree().change_scene("res://Level_menu.xml")
	set_mouse_visible()
	queue_free()

func on_ESC_pressed():
	set_mouse_visible()
	get_node("ParallaxBackground/PausePopUp").set_exclusive(true)
	get_node("ParallaxBackground/PausePopUp").popup()
	get_tree().set_pause(true)

func _on_Continuar_pressed():
	set_mouse_invisible()
	get_node("ParallaxBackground/PausePopUp").hide()
	get_tree().set_pause(false)

func _on_Salir_pressed():
	get_tree().set_pause(false)
	get_tree().change_scene("res://Main_menu.xml")
	queue_free()

func set_mouse_visible():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func set_mouse_invisible():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _on_spawn_goblin_signal():
	get_node("GoblinGenerator").spawn()

func _on_wave_end_reached_signal():
	wave_end_reached = true
