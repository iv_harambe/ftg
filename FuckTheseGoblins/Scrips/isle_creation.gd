
extends Node2D

var tile_map
var isle_pos
var used_tiles = []

func setup(tile_map_param, isle_pos_param):
	tile_map = tile_map_param
	isle_pos = isle_pos_param

const up = "top"
const down = "bottom"
const left = "left"
const right = "right"

func place_isle_with_bridge_long(center, bridges_long):
	used_tiles.append(center)
	var longest_orientation = get_orientations_in_order(center)
	call('_bridge_to_' + longest_orientation[0]['orientation'], center, bridges_long)
	call('_bridge_to_' + longest_orientation[1]['orientation'], center, bridges_long)

func get_orientations_in_order(pos):
	# Create value for each orientation
	var arri = {'value': pos.y, 'orientation': up}
	var aba = {'value': height_tiles - pos.y, 'orientation': down}
	var izq = {'value': pos.x, 'orientation': left}
	var der = {'value': width_tiles - pos.x, 'orientation': right}
	var orientations = [arri, aba, izq, der]
	# Return the sorted orientations
	orientations.sort_custom(self, 'orientation_dict_sorting')
	return orientations

func orientation_dict_sorting(elem_a, elem_b):
	return elem_a['value'] > elem_b['value']

func _bridge_to_top(center, max_bridge_long):
	var travel = Vector2(center.x, center.y)
	while(travel.y >= center.y - max_bridge_long):
		travel.y -= 1

		var travel_side_0 = Vector2(travel.x - 1, travel.y)
		used_tiles.append(travel_side_0)
		tiles_vector_with_water.append(travel_side_0)
		tile_map.set_cellv(travel_side_0, tile_map.get_tileset().find_tile_by_name("water_complete_right"))

		var travel_side_1 = Vector2(travel.x + 1, travel.y)
		used_tiles.append(travel_side_1)
		tiles_vector_with_water.append(travel_side_1)
		tile_map.set_cellv(travel_side_1, tile_map.get_tileset().find_tile_by_name("water_complete_left"))

		used_tiles.append(travel)
		tile_map.set_cellv(travel, tile_map.get_tileset().find_tile_by_name("bridge_vertical"))

	travel.y -= 1
	used_tiles.append(travel)

func _bridge_to_bottom(center, max_bridge_long):
	var travel = Vector2(center.x, center.y)
	while(travel.y <= center.y + max_bridge_long):
		travel.y += 1

		var travel_side_0 = Vector2(travel.x - 1, travel.y)
		used_tiles.append(travel_side_0)
		tiles_vector_with_water.append(travel_side_0)
		tile_map.set_cellv(travel_side_0, tile_map.get_tileset().find_tile_by_name("water_complete_right"))

		var travel_side_1 = Vector2(travel.x + 1, travel.y)
		used_tiles.append(travel_side_1)
		tiles_vector_with_water.append(travel_side_1)
		tile_map.set_cellv(travel_side_1, tile_map.get_tileset().find_tile_by_name("water_complete_left"))

		used_tiles.append(travel)
		tile_map.set_cellv(travel, tile_map.get_tileset().find_tile_by_name("bridge_vertical"))
	travel.y += 1
	used_tiles.append(travel)

func _bridge_to_left(center, max_bridge_long):
	var travel = Vector2(center.x, center.y)
	while(travel.x >= center.x - max_bridge_long):
		travel.x -= 1

		var travel_side_0 = Vector2(travel.x, travel.y - 1)
		used_tiles.append(travel_side_0)
		tiles_vector_with_water.append(travel_side_0)
		tile_map.set_cellv(travel_side_0, tile_map.get_tileset().find_tile_by_name("water_complete_bottom"))

		var travel_side_1 = Vector2(travel.x, travel.y + 1)
		used_tiles.append(travel_side_1)
		tiles_vector_with_water.append(travel_side_1)
		tile_map.set_cellv(travel_side_1, tile_map.get_tileset().find_tile_by_name("water_complete_top"))

		used_tiles.append(travel)
		tile_map.set_cellv(travel, tile_map.get_tileset().find_tile_by_name("bridge_horizontal"))
	travel.x -= 1
	used_tiles.append(travel)

func _bridge_to_right(center, max_bridge_long):
	var travel = Vector2(center.x, center.y)
	while(travel.x <= center.x + max_bridge_long):
		travel.x += 1

		var travel_side_0 = Vector2(travel.x, travel.y - 1)
		used_tiles.append(travel_side_0)
		tiles_vector_with_water.append(travel_side_0)
		tile_map.set_cellv(travel_side_0, tile_map.get_tileset().find_tile_by_name("water_complete_bottom"))

		var travel_side_1 = Vector2(travel.x, travel.y + 1)
		used_tiles.append(travel_side_1)
		tiles_vector_with_water.append(travel_side_1)
		tile_map.set_cellv(travel_side_1, tile_map.get_tileset().find_tile_by_name("water_complete_top"))

		used_tiles.append(travel)
		tile_map.set_cellv(travel, tile_map.get_tileset().find_tile_by_name("bridge_horizontal"))
	travel.x += 1
	used_tiles.append(travel)

