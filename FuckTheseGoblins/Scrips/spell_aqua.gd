extends RigidBody2D

var velocidad_disparo = 400
var danho = 150
var goblin_class = preload("Goblin.gd")
var rock_class = preload("rock.gd")

func shoot(player, mira):
	var pos = player.get_pos()
	var posicion_mira = (mira.get_pos() - player.get_pos()).normalized()
	var angulo_disparo = player.get_pos().angle_to(posicion_mira)
	_shoot(pos + posicion_mira * 50, posicion_mira, angulo_disparo)
	
	player.effects_player.play("fireball-effect-01")
	add_collision_exception_with(player)
	
func _shoot(pos , a_donde_disparar, angulo):
	set_pos(pos)
	get_node(".").rotate(angulo)
	set_linear_velocity(a_donde_disparar * velocidad_disparo)
	get_node("AnimSprite/Anim").play("form")
	
func _on_Area2D_body_enter( body ):
	if body extends goblin_class:
		get_node("Area2D").queue_free()
		body.recibir_danho(danho)
		call_deferred("queue_free")
	elif body extends rock_class:
		get_node("Area2D").queue_free()
		body.reduce(2)
		call_deferred("queue_free")
	elif body extends StaticBody2D:
		queue_free()

func _on_Area2D_body_exit( body ):
	pass
