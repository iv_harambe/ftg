
extends Node2D

var file = null
var wave_end_reached = false
onready var timer = get_node("Timer")

signal spawn_goblin
signal wave_end_reached

func start_wave(wave_number):
	wave_end_reached = false
	file = File.new()
	file.open("res://Waves Management/waves_file", file.READ)
	while not file.eof_reached() and not file.get_line().strip_edges().begins_with("wave " + str(wave_number)):
		pass
	parse_step()

func parse_step():
	if not file.eof_reached() and not wave_end_reached:
		parse_line(file.get_line().strip_edges())
	else:
		file.close()

func parse_line(string_line):
	var string_array = string_line.split(" ")
	if self.has_method(string_array[0]):
		var function = string_array[0]
		string_array.remove(0)
		self.call(function, string_array)
	else:
		parse_step()

func wait(array_wait_time):
	timer.set_wait_time(int(array_wait_time[0]))
	timer.start()

func _on_Timer_timeout():
	timer.stop()
	parse_step()

func output(array_to_print):
	print(array_to_print[0])
	parse_step()

func spawn(array_goblin_amount):
	for i in range(int(array_goblin_amount[0])):
		emit_signal("spawn_goblin")
	parse_step()

func end(array):
	wave_end_reached = true
	emit_signal("wave_end_reached")
	parse_step()
