extends Node2D

var spell
var costo = 0
var spell_shooter = preload("res://SpellShooter.xml")

func _ready():
	var aux = 'Valor: ' + str(costo)
	get_node("Valor").set_text(aux)
	
func _on_Button_pressed():
	if puede_comprar():
		var sp = Container.new()
		var image = Sprite.new()
		var spell_shooter_inst = spell_shooter.instance() 
		
		image.set_texture(get_node("Sprite 2").get_button_icon())
		image.set_name("SpellSprite")
		
		spell_shooter_inst.spell = spell
		spell_shooter_inst.set_name("SpellObject")
		
		sp.add_child(spell_shooter_inst)
		sp.add_child(image)
		
		game_state.Spells.append(sp)
		
		if Spells_Resourse.spell_selected == null:
			Spells_Resourse.spell_selected = spell_shooter_inst
		
	get_node("Button").set_disabled(true)
	
	game_state.goblin_mucus = game_state.goblin_mucus - costo
	
	get_node("StreamPlayer").play()
	
func puede_comprar():
	return game_state.goblin_mucus > costo