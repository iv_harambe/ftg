extends Container


func _ready():
	var posicion = Vector2(-164,-0.5)
	var sp = Container.new()
	
	for x in range (0, game_state.Spells.size()):
		var image = Sprite.new()
		image.set_texture(game_state.Spells[x].get_node("SpellSprite").get_texture())
		image.set_scale(Vector2(0.43,0.43))
		image.set_pos(posicion)
		
		add_child(image)
		posicion = Vector2(posicion.x + 32 , posicion.y)