
extends Position2D

func _ready():
	set_fixed_process(true)
	
func _fixed_process(delta):
	var mousepos = get_parent().get_parent().get_node("Mira").get_pos() - get_parent().get_pos() 
	var ang = get_parent().get_pos().angle_to(mousepos)
	
	set_pos((Vector2(0,0)).rotated(ang))