
extends Sprite

func _ready():
	var animation_to_play = randi() % 4
	randomize()
	get_node("AnimationPlayer").play("blood_" + str(animation_to_play))

func animation_end():
	queue_free()
