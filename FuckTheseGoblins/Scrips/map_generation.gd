
extends Node2D

var tilemap = null
var tilemap_tree = null
var ysort = null
var nivel = null
var map_width_tiles = null
var map_height_tiles = null

func setup(nivel_param, tilemap_param, tilemap_tree_param, y_sort):
	tilemap = tilemap_param
	tilemap_tree = tilemap_tree_param
	ysort = y_sort
	nivel = nivel_param

func _ready():
	create_map()

func create_map():
	update_map_size()
	fix_limits()
	fill_map()

func update_map_size():
	var total_tiles = int(20 * (1 + 0.20 * nivel))
	map_width_tiles = int(rand_range(total_tiles * 0.5, total_tiles * 0.7))
	map_height_tiles = total_tiles - map_width_tiles
	randomize()
	#print(map_width_tiles)
	#print(map_height_tiles)

func fix_limits():
	var limites = load("square_map.xml").instance()
	var map_width_pixels = map_width_tiles * tilemap.get_cell_size().x
	var map_height_pixels = map_height_tiles * tilemap.get_cell_size().y
	game_state.map_max_width = map_width_pixels
	game_state.map_max_height = map_height_pixels
	limites.setup(map_width_pixels, map_height_pixels)
	get_parent().add_child(limites)

func fill_map():
	var filler = load("map_filler.xml").instance()
	filler.setup(map_width_tiles, map_height_tiles, 5, tilemap, tilemap_tree, ysort)
	filler.set_name("MapFiller")
	get_parent().add_child(filler)
