extends Sprite

onready var ultima_vida_registrada = get_parent().vida
onready var vida_actual_node = get_node("Vida_Actual")
onready var initial_scale = vida_actual_node.get_scale().x

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	var vida_actual = get_parent().vida
	if ultima_vida_registrada != vida_actual:
		ultima_vida_registrada = vida_actual
		var ejex = initial_scale * (vida_actual / get_parent().vida_max)
		vida_actual_node.set_scale(Vector2(ejex, vida_actual_node.get_scale().y))