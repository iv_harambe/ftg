
extends Sprite

var player_class = preload("player.gd")
var cantidad_que_cura = 5

func _ready():
	randomize()
	set_frame(randi() % 15)

func _on_Area2D_body_enter( body ):
	if body extends player_class:
		body.curar(cantidad_que_cura)
		queue_free()
