
extends KinematicBody2D

# Other nodes

var navigation_node = null

# Definitions
var goblin_speed = 2
var presa = null
var danho = 1
var vida = 100
var velocidad_de_ataque = 0.5
var efecto = null
var elemento_danho = null
var puede_atacar = true
var choco_pared = false
var state = 'bottom'
var next_state = 'top'
func setup(navigation_node_param, presa_param):
	navigation_node = navigation_node_param
	presa = presa_param

func _ready():
	set_fixed_process(true)
	get_node("SonidosGob")._ready()

func _fixed_process(delta):
	atacar()
	perseguir()
	die()
	anim()

func where_is_looking():
	var pos_to = (presa.get_pos() - get_pos()).normalized()
	if sign(pos_to.y) == -1:
		if pos_to.x < -0.80:
			return "left"
		elif (pos_to.x > 0.80):
			return "right"
		else:
			return "top"
	else:
		if pos_to.x < -0.80:
			return "left"
		elif (pos_to.x > 0.80):
			return "right"
		else:
			return "bottom" 

func anim ():
	#Logica para la animación del movimiento
	#if (next_state == "left" and state != "left"):
	#	get_node("Sprite/Anim").play("left")
	if (next_state != state):
		state = next_state
		get_node("Sprite/Anim").play(state)
	next_state = where_is_looking()


func atacar():
	if(distancia_a_la_presa() < 50) and puede_atacar:
		presa.me_ataca_un_gob(self)
		puede_atacar = false
		get_node("velocidad_de_ataque").set_wait_time(velocidad_de_ataque)
		get_node("velocidad_de_ataque").start()

func distancia_a_la_presa():
	return get_pos().distance_to(presa.get_pos())

func set_presa(presa_param):
	presa = presa_param

func en_rango():
	return( (abs(self.get_pos().x - presa.get_pos().x) <= 25) and (abs(self.get_pos().y - presa.get_pos().y) <= 25))

func perseguir():
	var direction = null
	if en_rango() :
		direction = Vector2(0,0)
	else:
		if choco_pared:
			direction = direction_using_pathfinding()
		else:
			direction = (presa.get_pos() - get_pos()).normalized()
	#look_at(presa.get_pos())
	move(direction * goblin_speed)

func direction_using_pathfinding():
	var distance = Vector2(0.1,0.1)
	var points_to_follow = navigation_node.get_simple_path(get_global_pos(), presa.get_pos(), false)
	if points_to_follow.size() > 1:
		var distance_between_points = (points_to_follow[1] - points_to_follow[0]).length()
		if distance_between_points > 2.0:
			distance = points_to_follow[1] - get_global_pos()
		elif points_to_follow.size() > 2:
			distance = points_to_follow[2] - get_global_pos()
	return distance.normalized()

func choco_pared():
	choco_pared = true
	get_node("volver_a_perseguir").start()

func _on_volver_a_perseguir_timeout():
	choco_pared = false

func die():
	if is_colliding():
		var collider = get_collider()
		get_collider().impacte_goblin(self)

func recibir_danho(danho):
	vida -= danho
	if vida <= 0:
		dead()

func dead():
	var mucus = generate_goblin_mucus_amount()
	get_parent().get_parent().dead_goblin(get_pos(), mucus)
	self.queue_free()

func generate_goblin_mucus_amount():
	return (randi() % (10 - 5)) + 5

func impacte_goblin(goblin):
	return

func _on_velocidad_de_ataque_timeout():
	puede_atacar = true
	get_node("velocidad_de_ataque").stop()
