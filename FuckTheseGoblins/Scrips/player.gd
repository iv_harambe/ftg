
extends KinematicBody2D

# Children nodes
var spell = preload("res://spell.xml")
var blood = preload("res://blood_animation.xml")

var spell_number = 0

onready var timer_recarga_spell = get_node("Recarga_de_spell")
onready var timer_spellr = get_node("timer_spellr")
onready var effects_player = get_node("EffectsPlayer")
# Other nodes

onready var mira = get_node("../../Mira")

#Definitions
var player_speed_normal = 3.5
var tiempo_de_recarga = 0.5
var recargando = false
var spellr_enable = true
var spellr_cooldown = 5.0
var bait_enable = true
var vida = 10.0
var efecto_actual = null
var aura = null
var vida_max = 10.0
var orientacion_sprite = "idle"
var state = 'idle'
var next_state = 'idle'
signal dead_player
var freeButtonPrev = false
var freeButtonNext = false
var second_sp = true

func _ready():
	timer_recarga_spell.set_wait_time(tiempo_de_recarga)
	timer_spellr.set_wait_time(spellr_cooldown)
	set_fixed_process(true)

func _anim ():
	#Logica para la animación del movimiento
	#if (next_state == "left" and state != "left"):
	#	get_node("Sprite/Anim").play("left")
	if (next_state != state):
		state = next_state
		get_node("Sprite/Anim").play(state)

	if (next_state == "idle" and orientacion_sprite == "bottom" ):
		get_node("Sprite/Anim").play("idle")
	if (next_state == "idle" and orientacion_sprite == "left" ):
		get_node("Sprite/Anim").play("idle_left")
	if (next_state == "idle" and orientacion_sprite == "right" ):
		get_node("Sprite/Anim").play("idle_right")
	if (next_state == "idle" and orientacion_sprite == "up" ):
		get_node("Sprite/Anim").play("idle_top")

func _cambia_spells():
	
	if(Input.is_action_pressed("prev_spell")&&freeButtonPrev&&spell_number>0):
		var posicionX = get_node("../../ParallaxBackground/Spells/Selector").get_pos().x-32
		var posicionY = get_node("../../ParallaxBackground/Spells/Selector").get_pos().y
		get_node("../../ParallaxBackground/Spells/Selector").set_pos(Vector2(posicionX,posicionY))
		spell_number = spell_number - 1
		print(spell_number)
		Spells_Resourse.spell_selected = game_state.Spells[spell_number].get_node("SpellObject")
		print(Spells_Resourse.spell_selected)
		freeButtonPrev = false
	else:
		freeButtonPrev = true
	
	if(Input.is_action_pressed("next_spell")&&freeButtonNext&&spell_number<game_state.Spells.size()-1):
		var posicionX = get_node("../../ParallaxBackground/Spells/Selector").get_pos().x+32
		var posicionY = get_node("../../ParallaxBackground/Spells/Selector").get_pos().y
		get_node("../../ParallaxBackground/Spells/Selector").set_pos(Vector2(posicionX,posicionY))
		spell_number = spell_number + 1
		print(spell_number)
		Spells_Resourse.spell_selected = game_state.Spells[spell_number].get_node("SpellObject")
		print(Spells_Resourse.spell_selected)
		freeButtonNext = false
	else:
		freeButtonNext = true
func _shoot():
	# este metodo se encarga de generar el hechizo 
	# y lanzarlo o invocarlo segun requiera
	var mouse_left_button = Input.is_action_pressed("mouse_left_button")
	if(mouse_left_button and not recargando):
		#Posicion desde donde se dispara
		var pos = get_pos()
		#Se crea una instancia de Spell y se agrega al nodo
		var sp = spell.instance()
		get_parent().add_child(sp)
		#Se toma la posicion del casteo y se calcula en base
		#a la posicion de la mira el vector de disparo
		var posicion_mira = (mira.get_pos() - get_pos()).normalized()
		var angulo_disparo = get_pos().angle_to(posicion_mira)

		effects_player.play("fireball-effect-01")
		sp._shoot(pos + posicion_mira * 50, posicion_mira, angulo_disparo)
		timer_recarga_spell.start()
		recargando = true
		add_collision_exception_with(sp)

func _shootR():
	var mouse_right_button = Input.is_action_pressed("mouse_right_button")
	if (mouse_right_button&&Spells_Resourse.spell_selected!=null&&second_sp):
		Spells_Resourse.spell_selected.shoot(self,mira)
		get_node("timer_spellr").set_wait_time(3.0)
		get_node("timer_spellr").start()
		second_sp = false




func _move():
	var vector_mover_hacia = _vector_movimiento()
	move(vector_mover_hacia * player_speed_normal)
	#if _moviendo_hacia_mira(vector_mover_hacia):
	#	move(vector_mover_hacia * player_speed_normal)
	#else:
	#	move(vector_mover_hacia * player_speed_reduced)


func _vector_movimiento():
	next_state = 'idle'
	var x = 0
	var y = 0
	if Input.is_action_pressed("btn_left"):
		x = -1
		next_state = "left"
		orientacion_sprite = "left"
	elif Input.is_action_pressed("btn_right"):
		x = 1
		next_state = "right"
		orientacion_sprite = "right"
	if Input.is_action_pressed("btn_up"):
		y = -1
		next_state = 'up'
		orientacion_sprite = "up"
	elif Input.is_action_pressed("btn_down"):
		y = 1
		next_state = 'bottom'
		orientacion_sprite = "bottom"
	return Vector2(x, y)
	
	
func _moviendo_hacia_mira(vector):
	var pos_mira = mira.get_pos() - get_pos()
	var hacia_mira_en_x = sign(pos_mira.x) == sign(vector.x) or vector.x == 0
	var hacia_mira_en_y = sign(pos_mira.y) == sign(vector.y) or vector.y == 0
	return hacia_mira_en_x and hacia_mira_en_y

func _fixed_process(delta):
	_shoot()
	_shootR()
#	_bait()
#	_aqua_spell()
	_move()
	_anim()
	_cambia_spells()

func _on_Recarga_de_spell_timeout():
	recargando = false
	timer_recarga_spell.stop()
	
func impacte_goblin(goblin):
	return

func me_ataca_un_gob(goblin):
	print("me atacan gil laburante!!")
	var blood_instance = blood.instance()
	add_child(blood_instance)
	aplicar_efecto(goblin.efecto)
	aplicar_danho(goblin.danho,goblin.elemento_danho) 
	if(vida <= 0):
		emit_signal("dead_player")

func aplicar_efecto(efecto):
	if (efecto != null) and (efecto_actual == null):
		efecto_actual = efecto

func aplicar_danho(danho , elemento_del_danho):
	var danho_final = 1
	danho_final = danho * calcular_danho_elemental(elemento_del_danho)
	vida -= danho_final

func calcular_danho_elemental(elemento):
	if aura == null:
		return 1
	else:
		aura.get_danho(elemento)


func _on_timer_spellr_timeout():
	second_sp = true
	timer_spellr.stop()

func recibir_danho( damage ):
	pass

func curar(cantidad_a_curar):
	if (vida + cantidad_a_curar) > vida_max:
		vida = vida_max
	else:
		vida += cantidad_a_curar

func _on_bait_cooldown_timeout():
	bait_enable = true


