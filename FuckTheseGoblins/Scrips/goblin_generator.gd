
extends Node2D

var max_width = null
var max_height = null
var axis_list = ["x", "y"]
var distance_from_limit = 10
var prey = null

func _ready():
	max_width = game_state.map_max_width
	max_height = game_state.map_max_height

func spawn():
	var gob = game_state.lista_goblins.dar_gob(0)
	gob.set_pos(random_pos_near_limits())
	get_parent().agregar_goblin(gob)
	game_state.goblins += 1

func random_pos_near_limits():
	var vector = Vector2()
	# Generate axis to be random, and axis to be fixed
	randomize()
	var pos_to_be_random = randi() % 2
	var pos_to_be_fixed = (pos_to_be_random + 1) % 2
	var axis_to_be_random = axis_list[pos_to_be_random]
	var axis_to_be_fixed = axis_list[pos_to_be_fixed]
	var max_long = call("max_" + axis_to_be_random)
	# Generate random position in axis
	randomize()
	var random_axis_value = (randi() % int(max_long - (distance_from_limit * 2))) + distance_from_limit
	# Generate a segment to be fixed
	randomize()
	var fixed_axis_value = call("get_random_fixed_axis_" + axis_to_be_fixed)
	# Set vector with generated positions
	vector = call("set_vector_" + axis_to_be_random, vector, random_axis_value)
	vector = call("set_vector_" + axis_to_be_fixed, vector, fixed_axis_value)

	return vector

func get_random_fixed_axis_x():
	randomize()
	return [distance_from_limit, max_width - distance_from_limit][randi() % 2]

func get_random_fixed_axis_y():
	randomize()
	return [distance_from_limit, max_height - distance_from_limit][randi() % 2]

func max_x():
	return max_width

func max_y():
	return max_height

func set_vector_x(vector, value):
	vector.x = value
	return vector

func set_vector_y(vector, value):
	vector.y = value
	return vector
