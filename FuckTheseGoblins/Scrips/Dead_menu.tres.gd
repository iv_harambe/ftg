
extends Control



func _on_Main_menu_pressed():
	get_tree().change_scene("res://Main_menu.xml")


func _on_Restart_level_pressed():
	get_tree().change_scene("res://FTG.xml")


func _on_Restart_game_pressed():
	game_state.restart()
	get_tree().change_scene("res://FTG.xml")
