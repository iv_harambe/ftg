
extends Node2D

var width_tiles = null
var height_tiles = null

var tile_map = null
var tile_map_tree = null
var ysort = null

var resources_list = ["_water", "_rock"]
var max_amount_tiles = null
var used_tiles = []
var filled_tiles_amount = 0

var tiles_vector_with_water = []

func setup(width_tiles_param, height_tiles_param, percentage_to_fill, tilemap, tilemap_tree, y_sort):
	tile_map = tilemap
	tile_map_tree = tilemap_tree
	ysort = y_sort
	width_tiles = width_tiles_param
	height_tiles = height_tiles_param
	max_amount_tiles = width_tiles * height_tiles * 0.01 * percentage_to_fill

func _ready():
	fill_map()

func fill_map():
	fill_with_grass()
	fill_with_trees()
	place_isle(random_pos_for_isle())
	while filled_tiles_amount < max_amount_tiles:
		fill_pos_if_was_not_used(generate_random_pos())
		randomize()
	combine_water()

func fill_with_grass():
	for x in range(-3, width_tiles + 3):
		for y in range(-3, height_tiles + 3):
			tile_map.set_cell(x, y, tile_map.get_tileset().find_tile_by_name("grass"))

# Functions for filling the map with trees

func fill_with_trees():
	var range_x = range(-3, 0) + range(width_tiles, width_tiles + 4)
	var range_y = range(-3, -1) + range(height_tiles - 1, height_tiles + 4)

	for x in range_x:
		for y in range(-3, height_tiles + 3):
			tile_map_tree.set_cell(x, y, get_random_tree_id())

	for y in range_y:
		for x in range(-3, width_tiles + 3):
			tile_map_tree.set_cell(x, y, get_random_tree_id())

func get_random_tree_id():
	var tree_id = randi() % 3
	randomize()
	return tile_map_tree.get_tileset().find_tile_by_name("tree_" + str(tree_id))

# Functions for filling the map with other elements

func generate_random_pos():
	var pos_x = (randi() % (width_tiles - 2)) + 1
	var pos_y = (randi() % (height_tiles - 2)) + 1
	return Vector2(pos_x, pos_y)

func fill_pos_if_was_not_used(vector):
	if was_not_used(vector):
		used_tiles.append(vector)
		set_random_resource(vector)
		filled_tiles_amount += 1

func set_random_resource(vector):
	randomize()
	call(resources_list[randi() % resources_list.size()], vector)

func _water(vector):
	tiles_vector_with_water.append(vector)
	tile_map.set_cell(vector.x, vector.y, tile_map.get_tileset().find_tile_by_name("water"))

func _rock(vector):
	tile_map.set_cellv(vector, tile_map.get_tileset().find_tile_by_name("grass_no_navigation"))
	var rock_instance = load("rock.xml").instance()
	randomize()
	rock_instance.setup((randi() % 6) + 1)
	rock_instance.set_pos(get_tile_center_pos(vector))
	ysort.add_child(rock_instance)

func get_tile_center_pos(vector):
	var cell_size = tile_map.get_cell_size()
	var x = (vector.x * cell_size.x) + (cell_size.x / 2)
	var y = (vector.y * cell_size.y) + (cell_size.y / 2) - 10 # Substract so the rock won't be touching the possibly water in the under tile
	return Vector2(x, y)

func was_not_used(vector):
	for tile in used_tiles:
		if tile == vector:
			return false
	return true

# Functions for creating the isle

func random_pos_for_isle():
	var pos_x = (randi() % (width_tiles - 2)) + 2
	var pos_y = (randi() % (height_tiles - 2)) + 2
	return Vector2(pos_x, pos_y)

func place_isle(topleft_corner_pos):
	if there_is_space_for_island(topleft_corner_pos):
		place_isle_and_append_used_tiles(topleft_corner_pos)

func there_is_space_for_island(topleft_corner_pos):
	var there_is_space_in_variables_limits = topleft_corner_pos.x + 5 < width_tiles and topleft_corner_pos.y + 5 < height_tiles
	var there_is_space_in_origin_limits = topleft_corner_pos.x - 1 > 0 and topleft_corner_pos.y - 1 > 0
	return there_is_space_in_variables_limits and there_is_space_in_origin_limits

func place_isle_and_append_used_tiles(topleft_corner_pos):
	var isle_index = 0
	for y in range(topleft_corner_pos.y, topleft_corner_pos.y + 5):
		for x in range(topleft_corner_pos.x, topleft_corner_pos.x + 5):
			tile_map.set_cell(x, y, -1)
			used_tiles.append(Vector2(x, y))
			tile_map.set_cell(x, y, tile_map.get_tileset().find_tile_by_name('isle_' + str(isle_index)))
			isle_index += 1

# Functions for combining the water wells

func combine_water():
	for tile_vector in tiles_vector_with_water:
		var resource_name = resource_name_combined_with_tiles_around(tiles_around_with_water(tile_vector))
		var resource_id = tile_map.get_tileset().find_tile_by_name(resource_name)
		tile_map.set_cell(tile_vector.x, tile_vector.y, resource_id)

func tiles_around_with_water(vector):
	var tiles_around = []
	var resource_left = tile_map.get_cell(vector.x - 1, vector.y)
	if tile_map.get_tileset().tile_get_name(resource_left).begins_with("water"):
		tiles_around.append("left")
	var resource_right = tile_map.get_cell(vector.x + 1, vector.y)
	if tile_map.get_tileset().tile_get_name(resource_right).begins_with("water"):
		tiles_around.append("right")
	var resource_top = tile_map.get_cell(vector.x, vector.y - 1)
	if tile_map.get_tileset().tile_get_name(resource_top).begins_with("water"):
		tiles_around.append("top")
	var resource_bottom = tile_map.get_cell(vector.x, vector.y + 1)
	if tile_map.get_tileset().tile_get_name(resource_bottom).begins_with("water"):
		tiles_around.append("bottom")

	return tiles_around

func resource_name_combined_with_tiles_around(tiles_around):
	var left_in = "left" in tiles_around
	var right_in = "right" in tiles_around
	var top_in = "top" in tiles_around
	var bottom_in = "bottom" in tiles_around
	var final_resource = "water_"
	
	if left_in:
		if right_in:
			if top_in:
				return final_resource + "t_top"
			elif bottom_in:
				return final_resource + "t_bottom"
			else:
				return final_resource + "column_horizontal"
		elif top_in:
			if bottom_in:
				return final_resource + "t_left"
			else:
				return final_resource + "corner_bottomright"
		elif bottom_in:
			return final_resource + "corner_topright"
		else:
			return final_resource + "combined_left"

	if right_in:
		if top_in:
			if bottom_in:
				return final_resource + "t_right"
			else:
				return final_resource + "corner_bottomleft"
		elif bottom_in:
			return final_resource + "corner_topleft"
		else:
			return final_resource + "combined_right"

	if top_in:
		if bottom_in:
			return final_resource + "column_vertical"
		else:
			return final_resource + "combined_top"

	if bottom_in:
		return final_resource + "combined_bottom"

	return "water"
