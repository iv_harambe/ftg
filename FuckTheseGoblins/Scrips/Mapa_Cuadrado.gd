
extends Node2D

const VERTICAL = "Vertical"
const HORIZONTAL = "Horizontal"

var altura_mapa = null
var ancho_mapa = null

func setup(ancho, altura):
	ancho_mapa = ancho
	altura_mapa = altura

func _ready():
	_crear_rectangulo(ancho_mapa, altura_mapa)

#Crea un cuadrado a partir de un ancho y un alto
func _crear_rectangulo(ancho, alto):
	_nueva_pared(HORIZONTAL, ancho, Vector2(0,0))
	_nueva_pared(HORIZONTAL, ancho, Vector2(0, alto))
	_nueva_pared(VERTICAL, alto, Vector2(0,0))
	_nueva_pared(VERTICAL, alto, Vector2(ancho, 0))

#Crea una pared con la orientacion, una longitud, y una posicion
#Parametros:
#	orientacion = puede ser HORIZONTAL o VERTICAL
#	longitud_param = La longitud de la pared en pixeles
#	pos = La posicion a partir de la cual se creara la pared
func _nueva_pared(orientacion, longitud_param, pos):
	var rectangleshape_new_node = RectangleShape2D.new()
	var staticbody_new_node = StaticBody2D.new()
	var sprite_new_node = Sprite.new()
	var longitud = longitud_param / 2

	if orientacion == VERTICAL:
		var vector_extents = Vector2(1,longitud)
		rectangleshape_new_node.set_extents(vector_extents)
		var vector_scale = Vector2(vector_extents.x, vector_extents.y * 2)
		sprite_new_node.set_scale(vector_scale)
		staticbody_new_node.set_pos(Vector2(pos.x, pos.y + longitud))
	elif orientacion == HORIZONTAL:
		var vector_extents = Vector2(longitud,1)
		rectangleshape_new_node.set_extents(vector_extents)
		var vector_scale = Vector2(vector_extents.x * 2, vector_extents.y)
		sprite_new_node.set_scale(vector_scale)
		staticbody_new_node.set_pos(Vector2(pos.x + longitud, pos.y))

	staticbody_new_node.add_shape(rectangleshape_new_node)

#	sprite_new_node.set_texture(preload("../Sprites/pixel_rojo.jpg"))
#	sprite_new_node.set_centered(true)
	staticbody_new_node.add_child(sprite_new_node)
	staticbody_new_node.set_script(load("Scrips/Limite.gd"))

	add_child(staticbody_new_node)
