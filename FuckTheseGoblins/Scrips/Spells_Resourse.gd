
extends Node

var spell0 = preload("res://spell.xml")
var spell1 = preload("res://spell.xml")
var spell2 = preload("res://spell_aqua.xml")
var spell3 = preload("res://Heal.xml")
var spell4 = preload("res://Heal.xml")
var spell5 = preload("res://Heal.xml")
var spell6 = preload("res://spellR.xml")

var spellTexture0 = preload("res://Sprites/Menu/Spells/fireball-red-2.png")
var spellTexture1 = preload("res://Sprites/Menu/Spells/fireball-red-3.png")
var spellTexture2 = preload("res://Sprites/Menu/Spells/wind-blue-3.png")
var spellTexture3 = preload("res://Sprites/Menu/Spells/heal-jade-1.png")
var spellTexture4 = preload("res://Sprites/Menu/Spells/heal-jade-2.png")
var spellTexture5 = preload("res://Sprites/Menu/Spells/heal-jade-3.png")
var spellTexture6 = preload("res://Sprites/Menu/Spells/waterDragon.png")

var spellbut1 = preload("res://Spell_basic_Menu.xml")

var spells = []

var spell_selected = null

var sb = null
var pages = []

var content = ""

func loadText(spellNumber):
	var file = File.new()
	
	file.open("res://Texts/Menus/spell" + str(spellNumber) + ".data", file.READ)

	while!(file.eof_reached()):
		content = content + "\n" + file.get_line()
	file.close()
	return content

func cargarHechizo(spell,textura,costo,desc):
	
	sb = spellbut1.instance()
	content = ""
	
	sb.spell = spell
	sb.get_node("Sprite 2").set_button_icon(textura) 
	sb.costo = costo
	sb.get_node("Descripcion").set_text(desc)
	
	spells.append(sb)
	
	
func _ready():
	
	cargarHechizo(spell0,spellTexture0,100,loadText(0))
	cargarHechizo(spell1,spellTexture1,200,loadText(1))
	cargarHechizo(spell2,spellTexture2,200,loadText(2))
	cargarHechizo(spell3,spellTexture3,100,loadText(3))
	cargarHechizo(spell4,spellTexture4,100,loadText(4))
	cargarHechizo(spell5,spellTexture5,100,loadText(5))
	cargarHechizo(spell6,spellTexture6,500,loadText(6))
	
	var posicion = Vector2(250,100)
	var page = []
	
	for x in range(0, spells.size()):
		var sp = spells[x]
		sp.set_pos(posicion)
		page.append(sp)
		posicion = Vector2(posicion.x,posicion.y + 170)
		if (x%3 == 2)|| x == (spells.size()-1):
			posicion = Vector2(250,100)
			var sc = Container.new()
			for y in range(0,page.size()):
				sc.add_child(page[y])
			pages.append(sc)
			page = []


