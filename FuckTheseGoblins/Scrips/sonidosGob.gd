
extends StreamPlayer

func _ready():
	sonido()
	set_fixed_process(true)
	
	
func _fixed_process(delta):
	set_volume_db(10-get_parent().distancia_a_la_presa()/10)

func _on_Timer_timeout():
	sonido()

func sonido():
	set_stream(SoundResource.goblins[round(rand_range(0,SoundResource.goblins.size()-1))])
	get_node("Timer").set_wait_time(rand_range(0.5,SoundResource.tiempogob))
	get_node("Timer").start()
	play()