extends Node2D


var presa  
func _ready():
	game_state.goblins +=1
	set_fixed_process(true)

func invocar_Gobs():
		var gob = game_state.lista_goblins.dar_gob(0)
		gob.set_pos(get_pos())
		get_parent().agregar_goblin(gob)
		gob.set_presa(presa)
		game_state.goblins += 1

func _on_Timer_to_spawn_timeout():
	invocar_Gobs()


func _on_Timer_to_finish_wave_timeout():
	get_node("Timer_to spawn").set_autostart(false)
	get_node("Timer_to spawn").stop()
	game_state.goblins -=1
	self.queue_free()
