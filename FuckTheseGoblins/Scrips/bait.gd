
extends Sprite

var goblin_class = preload("Goblin.gd")
var in_area = []
var player = null
var bait_duration = 3

func setup(player_node, position):
	player = player_node
	set_pos(position)

func _ready():
	get_node("end_bait").set_wait_time(bait_duration)
	get_node("end_bait").start()

func impacte_goblin(goblin):
	pass

func me_ataca_un_gob(goblin):
	pass

func _on_end_bait_timeout():
	for goblin in in_area:
		if(goblin.get_ref()):
			goblin.get_ref().set_presa(player)
	queue_free()

func _on_bait_body_enter( body ):
	if body extends goblin_class:
		body.set_presa(self)
		in_area.append(weakref(body))

