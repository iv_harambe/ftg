
extends RichTextLabel

var vanish_enable = false

func setup(amount_to_show, image_texture, pos):
	add_image(image_texture)
	add_text(" " + str(amount_to_show))
	set_pos(pos)

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	move_up()
	if vanish_enable:
		vanish()
	remove_if_opacity_is_zero()

func move_up():
	var new_pos = Vector2(get_pos().x, get_pos().y - get_process_delta_time())
	set_pos(new_pos)

func vanish():
	set_opacity(get_opacity() * 0.95)

func remove_if_opacity_is_zero():
	if get_opacity() <= 0.1:
		queue_free()

func _on_wait_to_vinish_timeout():
	vanish_enable = true
