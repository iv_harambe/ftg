
extends StaticBody2D

# Children nodes
onready var sprite = get_node("Sprite")
onready var collision_shape = get_node("CollisionShape2D")

# Sprites' Rect2
var rock_6 = Rect2(0, 0, 55, 85)
var rock_5 = Rect2(68, 0, 55, 85)
var rock_4 = Rect2(141, 0, 55, 85)
var rock_3 = Rect2(0, 85, 55, 85)
var rock_2 = Rect2(70, 85, 55, 85)
var rock_1 = Rect2(142, 85, 55, 85)

var actual_state = null

func setup(initial_state):
	actual_state = initial_state

func _ready():
	set_sprite_for_actual_state()
	reduce_collision_shape_if_needed()

func reduce(amount):
	get_node("SamplePlayer2D").play("rock-smash")
	actual_state -= amount
	if actual_state <= 0:
		queue_free()
	else:
		set_sprite_for_actual_state()
		reduce_collision_shape_if_needed()

func set_sprite_for_actual_state():
	sprite.set_region_rect(get("rock_" + str(actual_state)))

func reduce_collision_shape_if_needed():
	if actual_state == 1:
		collision_shape.get_shape().set_radius(10)

func impacte_goblin(g):
	g.choco_pared()
